//TODO get it to work with multiple additions.  for instance 1 + 2 + 3


var answerDiv = document.getElementsByClassName("answer");

function isint(num) {

    return typeof num === "number";
}

//haven't checked to make sure it is a number yet
function add(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne + numTwo;
    }
    else {
        return false;
    }
}

function multiply(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne * numTwo;
    }
    else {
        return false;
    }
}

function divide(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne / numTwo;
    }
    else {
        return false;
    }
}

function subtract(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne - numTwo;
    }
    else {
        return false;
    }
}

var firstNum = [];
var secondNum = [];
var operator = "";
var mathArray = [];

//add all the numbers

$(".calculatorButtons").on("click", function (evt) {
    if (operator === "") {
        firstNum.push(evt.target.id);

    } else {
        secondNum.push(evt.target.id);
    }
    document.getElementById("answerDiv").value += evt.target.id;
});


$(".clear").on("click",function(){
    console.log("this");
    document.getElementById("answerDiv").value = "";
    clear();
});

//add all the operands
$(".operand").on("click", function (evt) {
    var value = evt.target.id;
    var answer;
    if (value === "equals") {
        mathArray.push(firstNum, operator, secondNum);
        //get numbers as opposed to arrays
        var first = arrayToInt(firstNum);
        var second = arrayToInt(secondNum);

        //do the maths here
        switch (operator) {
            case "multiply":
                answer = multiply(first, second);
                break;
            case "subtract":
                answer = subtract(first, second);
                break;
            case "add":
                answer = add(first, second);
                break;
            case "divide":
                answer = divide(first, second);
                break;

        }
    clear();


        document.getElementById("answerDiv").value = answer;


    } else {
        operator = value;
        document.getElementById("answerDiv").value = "";
    }
});

function clear(){
    firstNum =[];
    secondNum =[];
    mathArray = [];
    operator = "";
}


function arrayToInt(numArray) {
var number = "";
    numArray.forEach(function (num) {
       number += num;
    });


    return parseInt(number);
}

function printToDisplay(answer) {
    answerDiv.innerHTML = answer;
}


$("#cancel").click(function () {
    mathArray = [];
});




/**
 *
 *  @param numArray
 *  @returns {number}
 **/
function my_max(numArray) {

    var highest = 0;
    numArray.forEach(function (num) {
        if (num > highest) {
            highest = num;
        }
    });
    return highest;
}
/**
 *
 * @param text
 * @returns {number}
 */
function vowel_count(text) {
    var vowels = ["A", "E", "I", "O", "U", "Y"];
    var vowelCount = 0;
    for (var i = 0; i < text.length; i++) {
        if (jQuery.inArray(text.charAt(i).toUpperCase(), vowels) !== -1) {
            vowelCount++;
        }
    }
    return vowelCount;
}
/**
 *
 * @param reversable
 * @returns {string}
 */
function reverseString(reversable) {
    var result = "";
    for (var i = reversable.length - 1; i > -1; i--) {
        result += reversable.charAt(i);
    }
    return result;
}


//return highest number in an array
console.log(my_max([1, 2, 32, 4]));
//return the amount of vowels in a string
console.log(vowel_count("SEVEN"));
//return a reversed version of the string
console.log(reverseString("this is a string"));
